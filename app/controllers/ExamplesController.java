package controllers;

import akka.Done;
import akka.stream.Graph;
import akka.stream.OverflowStrategy;
import akka.stream.javadsl.Flow;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import akka.stream.javadsl.SourceQueueWithComplete;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.EventSource;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.WebSocket;

import java.util.*;
import java.util.concurrent.CompletionStage;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class ExamplesController extends Controller {

    /**
     * An action that returns a JSON object, to show how Jackson and Play JSON are
     * used to construct a JSON response
     * @return
     */
    public Result returnJson() {
        ObjectNode json = Json.newObject();
        json.put("Hello", "World");
        ArrayNode arr = json.putArray("names");
        arr.add("Alice");
        arr.add("Bob");
        arr.add("Charlie");

        return ok(json);
    }

    /**
     * To receive a JSON object from the body of a request, you can get it with
     * request().body().asJson()
     * @return
     */
    public Result receiveJsonPost() {

        JsonNode json = request().body().asJson();

        String name = json.findPath("name").asText();

        return ok("The name was " + name);

    }

    private String chars = "abcdefghijklmnopqrstuvwxyz0123456789";

    private Random random = new Random(System.currentTimeMillis());

    /**
     * Generates an 8 character random string
     * @return
     */
    private String randomId() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 8; i++) {
            int idx = random.nextInt(chars.length());
            sb.append(chars.charAt(idx));
        }
        return sb.toString();
    }


    public Result game(String id) {
        if (id != null) {
            return ok("The id was " + id);
        } else {
            return redirect("/example/game?id=" + randomId());
        }
    }

    List<SourceQueueWithComplete<String>> queues = new ArrayList<>();

    {
        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                String s = "The time is now " + System.currentTimeMillis();

                for (SourceQueueWithComplete<String> queue : queues) {
                    queue.offer(s);
                }
            }
        };
        timer.schedule(timerTask, 1000, 1000);
    }


    /**
     * An example websocket
     */
    public WebSocket webSocket() {
        return WebSocket.Text.accept(request ->


            // A websocket is made from a "flow", which has an input and an output
            Flow.fromSinkAndSourceCoupledMat(
                // First, the input. Sink.foreach lets us script what to do with every incoming message
                Sink.foreach(msg -> {
                    // let's just print the messages out
                    System.out.println("Message recieved: " + msg);
                }),

                // Next, what is the source of output going to be? This says it'll be a queue that we can push
                // messages to
                Source.queue(50, OverflowStrategy.backpressure()),

                // This lambda is called when the flow starts. The second parameter is our queue.
                // The first parameter is a "CompletionStage" (Java's version of a Promise) that will complete when
                // the socket is closed.
                (done, queue) -> {
                    // As soon as we have the queue, add it to the list of queues to push ticks out on
                    queues.add(queue);

                    // When the socket closes, remove the queue from the list of queues to push ticks out on
                    done.whenComplete((success, error) -> queues.remove(queue));
                    return done;
                }
            )
        );
    }


    /**
     * EventSource example. Not supported on Edge
     * @return
     */
    public Result eventSource() {

        // We're using an asynchronous queue as the source. What we get back is a "source" - the actual queue
        // will be created for us when the flow starts.
        Source<String, SourceQueueWithComplete<String>> source = Source.queue(50, OverflowStrategy.backpressure());

        // An eventsource is a chunked response
        return ok().chunked(
            // For each string from the source, create an event. This produces a source of events.
            // viaMat "materialises" this when the eventsource starts. What we do is in the callback
            source.map((str) -> EventSource.Event.event(str)).viaMat(EventSource.flow(), (queue, done) -> {
              // In our callback, the first argment is the queue. Now we have an actual queue we can push messages to.
              // Let's put it in the list of queues so the tickTimer (above) will push messages to it.
              queues.add(queue);
              return done;
          })
        ).as("text/event-stream");
    }

}
