name := """play-java-seed"""
organization := "cosc360"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.4"

resolvers += Resolver.url("Hopper releases", url("http://hopper.une.edu.au/artifactory/libs-release/"))

resolvers += Resolver.url("Hopper snapshots", url("http://hopper.une.edu.au/artifactory/libs-snapshot"))

libraryDependencies += guice

libraryDependencies += "com.typesafe.play" %% "play-json" % "2.6.10"