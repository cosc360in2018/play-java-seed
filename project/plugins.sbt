resolvers += Resolver.url("Hopper releases", url("http://hopper.une.edu.au/artifactory/libs-release/"))

resolvers += Resolver.url("Hopper snapshots", url("http://hopper.une.edu.au/artifactory/libs-snapshot"))

// The Play plugin
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.6.18")
